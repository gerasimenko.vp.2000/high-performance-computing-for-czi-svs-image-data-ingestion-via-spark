import os
import time

import numpy as np
from PIL import Image
from aicspylibczi import CziFile
from matplotlib import pyplot as plt
import re
from enum import Enum

import xml_parser


class CziProcessor:
    class Mode(Enum):
        SAVE_IMAGE = 0,
        SAVE_META_INFO = 1

    @staticmethod
    def __create_name(file_name: str, params: tuple):
        """
        Create name from initial czi file name and czi parameter

        :param str file_name: initial czi file name
        :param tuple params: czi parameters (dimensions, region)
        :return str: created name as string
        """

        dimensions, region = params
        name = file_name
        for dim_name, dim_value in dimensions.items():
            name += '_' + dim_name + '-' + str(dim_value)
        for coord in region:
            name += '_' + str(coord)
        return name

    @staticmethod
    def __extract_mosaics_image_data(row_of_data_frame, folder_to_save: str):
        """
        Extract image from czi file based on passed meta information and save this image

        :param dataframe row_of_data_frame: row_of_data_frame with czi file and meta information
        :param str folder_to_save: name of folder where save extracted image
        :return:
        """

        folder_name, file_name = row_of_data_frame['folder_name'], row_of_data_frame['file_name']
        dimensions, region = row_of_data_frame['dimensions'], row_of_data_frame['region']

        czi_file = CziFile(os.path.join(folder_name, file_name))

        mosaic_data = czi_file.read_mosaic(region=region, scale_factor=1.0, **dimensions)

        idx = [0] * (mosaic_data.ndim - 2) + [slice(None)] * 2  # we need only two last dimensions, all previous dimensions - take 0
        CziProcessor.__save_image(folder_to_save, CziProcessor.__create_name(file_name, (dimensions, region)),
                                  mosaic_data[idx])

    @staticmethod
    def __save_image(folder_name: str, file_name: str, mosaic_data: np.ndarray, ext: str = 'png'):
        """
        Save image represented as 2D numpy.array

        :param str folder_name: name of folder where to save image
        :param str file_name: name of file where to save image
        :param np.ndarray mosaic_data: image as 2D numpy.array
        :param str ext: extension of file
        :return:
        """

        file_name_with_ext = file_name + '.' + ext
        img = Image.fromarray(mosaic_data.astype(np.uint16))
        plt.imsave(os.path.join(folder_name, file_name_with_ext), img)

    @staticmethod
    def extract_mosaic_czi_files_from_folder(folder: str):
        """
        Extract only mosaic czi files from folder

        :param str folder: name of folder from where extract mosaic czi files
        :return list[str]: list of names of mosaic czi files
        """

        def get_file_ext(file):
            _, file_extension = os.path.splitext(file)
            return file_extension

        all_files = os.listdir(folder)
        return [file for file in all_files if
                get_file_ext(file) == '.czi' and CziFile(os.path.join(folder, file)).is_mosaic()]

    @staticmethod
    def process_mosaic_files(spark_context, folder_with_czi_files: str, folder_to_save: str, mode=Mode.SAVE_IMAGE):
        """
        Main method. Extract mosaic czi files from folder. Foreach extracted file get all mosaic images and save them

        :param mode:
        :param spark_context: initialized spark context
        :param str folder_with_czi_files: name of folder with czi files
        :param str folder_to_save: name of folder where to save extracted images
        :return float: total time of execution
        """

        def convert_bbox_to_dict(bbox):
            return bbox.x, bbox.y, bbox.w, bbox.h

        def create_generator(folder_name: str, czi_files: list[str], mode):
            """
            Create generator foreach mosaic_bounding_box foreach czi file

            :param mode:
            :param str folder_name: name of folder with czi_files
            :param list[str] czi_files: list of names of czi files
            :return:
            """
            for czi_file in czi_files:
                file = os.path.join(folder_name, czi_file)
                czi = CziFile(file)

                # user info is tuple
                user_info = xml_parser.XmlParser.parse_user_info(czi)

                mosaic_bounding_boxes: dict = czi.get_all_mosaic_tile_bounding_boxes()

                dimension_names = frozenset(('Z', 'C', 'T', 'R', 'I', 'H', 'V'))
                for tle, bbox in mosaic_bounding_boxes.items():
                    dimensions = {key: value for (key, value) in tle.dimension_coordinates.items() if
                                  key in dimension_names}
                    region = convert_bbox_to_dict(bbox)

                    if mode == CziProcessor.Mode.SAVE_META_INFO:
                        subblocks_metadata = czi.read_subblock_metadata(unified_xml=False, **dimensions)

                        for subblock_metadata in subblocks_metadata:
                            meta_data = subblock_metadata[1]
                            acquisition_time_pattern = "<AcquisitionTime>(.*?)</AcquisitionTime>"
                            found_by_pattern = re.search(acquisition_time_pattern, meta_data)
                            acquisition_time_value = '' if found_by_pattern is None else found_by_pattern.group(1)

                            yield folder_name, czi_file, dimensions, region, acquisition_time_value, user_info

                    else:
                        yield folder_name, czi_file, dimensions, region

        start_time = time.time()

        czi_files: list[str] = CziProcessor.extract_mosaic_czi_files_from_folder(folder_with_czi_files)
        assert czi_files, 'there is no mosaic czi files in directory'

        generator = create_generator(folder_with_czi_files, czi_files, mode)

        if mode == CziProcessor.Mode.SAVE_META_INFO:
            spark_context.parallelize(generator).toDF(['folder_name', 'file_name', 'dimensions', 'region', 'acquisition_time', 'user_info']) \
                .write.mode('overwrite').parquet(folder_to_save)
        else:
            spark_context.parallelize(generator).toDF(['folder_name', 'file_name', 'dimensions', 'region']).foreach(
                lambda x: CziProcessor.__extract_mosaics_image_data(x, folder_to_save))

        end_time = time.time()

        return end_time - start_time
