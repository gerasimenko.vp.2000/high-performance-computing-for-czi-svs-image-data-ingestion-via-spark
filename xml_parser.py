class XmlParser:
    @staticmethod
    def parse_user_info(czi_document):
        root = czi_document.meta

        for child in root:
            information = child.find('Information')

            user = information.find('User')

            attr_to_search = ['DisplayName', 'FirstName', 'MiddleName', 'LastName']
            len_of_attr = len(attr_to_search)
            found_attr_values = ['' for _ in range(len_of_attr + 1)]

            if user is not None:
                user_id = user.get('Id')
                found_attr_values[0] = user_id

                for i in range(len_of_attr):
                    found_attr = user.find(attr_to_search[i])

                    if found_attr is not None:
                        found_attr_values[i+1] = found_attr.text

        return found_attr_values
